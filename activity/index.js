let trainer = {};

//properties
trainer.name = 'Ash Ketchum';
trainer.age = 10;
trainer.pokemon = ['Pikachu', 'Squirtle', 'Charizard', 'Bulbasaur'];
trainer.friends = {
	kanto: ['Brock', 'Misty'],
	hoenn: ['May', 'Max']
}

//Methods
trainer.talk = function(){
	console.log("Pikachu, I choose you!")
}
trainer.talk();

//Check if all props are added
console.log(trainer);

//Access object properties using dot notation
console.log("Result of dot notation:");
console.log(trainer.name);

// Access object propertiesusing square brackets
console.log("Result of square bracket:");
console.log(trainer['pokemon']);

//Access the trainer "talk" method
console.log("Result of talk method");
trainer.talk();

//create constructor function for creating a pokemon
function Pokemon(name, level){

	//Properties
	this.name = name;
	this.level = level;
	this.health = 2 * level;
	this.attack = level;

	//Methods
	this.tackle = function(target){

		//reduces the target object's health property by subtracting and reassigning its value to the pokemon's attack
		console.log(this.name + " tackled " + target.name);
		target.health -= this.attack;

		console.log(target.name + "'s health is not reduced to " + target.health)

		if (target.health <= 0){
			target.faint()
		}
	};

	this.faint = function(){
		console.log(this.name + " fainted");
	}
}

// Instantiate a new pokemon
let pikachu = new Pokemon("Pikachu", 12);
console.log(pikachu);

//
let raichu = new Pokemon("Raichu", 15);
console.log(raichu);

let missingno = new Pokemon("Missingno", 18);
console.log(missingno);

//invoke the tackle method
raichu.tackle(pikachu);
console.log(pikachu);

//
missingno.tackle(pikachu)
console.log(missingno);